# Youtube Thumbnail gRPC service

### Сервис позволяет сохранять превью ютуб роликов по их URL

## Для запуска:

1. Склонировать репозиторий:
```bash
git clone https://github.com/frog-in-fog/youtube_thumbnail_grpc.git
```

2. Запустить миграцию:
```bash
cd server/cmd/migrator
```

```bash
make migrate
```
3. Синхронизировать зависимости
```bash
cd client
```

```bash
go mod tidy
```

```bash
cd server
```

```bash
go mod tidy
```
4. При неудачном импорте, вручную импортировать библиотеку с протофайлами и сгенерированными файлами по ссылке https://github.com/frog-in-fog/youtube_thumbnail_protos:
```bash
cd server
```
```bash
go get github.com/frog-in-fog/youtube_thumbnail_protos
```
5. Запустить сервер (демон докера должен быть запущен):

-  с помощью Make:
```bash
cd server
```

```
make server_build
```

-  с помощью docker compose:
```bash
cd server
```

```
docker-compose up --build -d
```

6. Собрать исполняемый файл клиента (golang должен быть установлен на машине):
```bash
cd client
```

```
make client_build
```

7. Запустить клиента (флаг async опциональный, позволяет загружать ролики асинхронно, остальные флаги - обязательные):
```bash
cd client/bin
```

```
./client --async --env=local --host=localhost --port=50051 <ссылка на ютуб ролик 1> <ссылка на ютуб ролик 2> <ссылка на ютуб ролик n>
```

После успешного завершения, превью с расширением .jpg будут сохранены в папке bin/thumbnails.

Также можно запустить тесты:
```bash
cd server
```

```bash
make test
```

или

```bash
go test ./tests
```
