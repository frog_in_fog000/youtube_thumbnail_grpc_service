package main

import (
	"bufio"
	"context"
	"flag"
	"fmt"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/client/internal/clients/grpc_client"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/client/lib/logger"
	"log/slog"
	"os"
	"path"
	"path/filepath"
	"strings"
	"sync"
)

func main() {
	env := flag.String("env", "", "environment, e.g. local, env, prod")
	host := flag.String("host", "", "address to listen")
	port := flag.Int("port", 0, "port to listen")
	async := flag.Bool("async", false, "download thumbnails asynchronously")
	flag.Parse()

	if *env == "" || *port == 0 || *host == "" {
		panic("env, host, grpcPort flags must be set")
	}

	log := logger.SetupLogger(*env)

	ctx := context.Background()

	videoUrls := flag.Args()
	if len(videoUrls) == 0 {
		log.Error("no video URLs provided")
	}

	log.Info("len before: ", len(videoUrls))

	uniqueVideoUrls := removeDuplicates(videoUrls)

	log.Info("len after: ", len(uniqueVideoUrls))

	log.Info("Client app started")

	addr := fmt.Sprintf("%s:%d", *host, *port)

	client, err := grpc_client.NewClient(ctx, addr, log)
	if err != nil {
		log.Error("failed to create client")
	}

	if *async {
		var wg sync.WaitGroup
		for _, videoUrl := range uniqueVideoUrls {
			wg.Add(1)
			go func(url string) {
				defer wg.Done()
				if err = handleTbn(ctx, client, url, log); err != nil {
					log.Error("failed to handle thumbnail", "err", err)
					return
				}
			}(videoUrl)
		}
		wg.Wait()
	} else {
		for _, videoUrl := range uniqueVideoUrls {
			if err = handleTbn(ctx, client, videoUrl, log); err != nil {
				return
			}
		}
	}

	log.Info("Client app finished successfully")
	log.Info("Thumbnails saved into thumbnails folder successfully")
}

// removeDuplicates removes duplicate args
func removeDuplicates(urls []string) []string {
	urlsMap := make(map[string]struct{}, len(urls))
	uniqueUrls := make([]string, 0, len(urls))

	for _, videoUrl := range urls {
		if _, exists := urlsMap[videoUrl]; !exists {
			urlsMap[videoUrl] = struct{}{}
			uniqueUrls = append(uniqueUrls, videoUrl)
		}
	}

	return uniqueUrls
}

// handleTbn makes request to server and saves images to the thumbnails folder
func handleTbn(ctx context.Context, client *grpc_client.Client, url string, log *slog.Logger) error {
	tbn, err := client.GetThumbnail(ctx, url)
	if err != nil {
		log.Error("failed to get thumbnail")
		return err
	}

	//fileName := time.Now().String() + ".jpeg"
	baseURL := path.Base(url)
	fileName := strings.TrimPrefix(baseURL, "watch?v=") + ".jpeg"
	filePath := path.Join("thumbnails", fileName)

	if err = os.MkdirAll(filepath.Dir(filePath), os.ModePerm); err != nil {
		log.Error("failed to create directories", "err", err)
		return err
	}

	file, err := os.OpenFile(filePath, os.O_RDWR|os.O_CREATE, 0666)
	if err != nil {
		log.Error("failed to open file", "err", err)
		return err
	}
	defer file.Close()

	w := bufio.NewWriter(file)

	_, err = w.Write(tbn)
	if err != nil {
		log.Error("failed to write thumbnail into file")
		return err
	}

	return w.Flush()
}
