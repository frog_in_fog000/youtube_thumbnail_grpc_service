package grpc_client

import (
	"context"
	yt_tbv1 "github.com/frog-in-fog/youtube_thumbnail_protos/gen/yt_thumbnail/yt_thumbnail"
)

// GetThumbnail handler for gRPC api method
func (c *Client) GetThumbnail(ctx context.Context, url string) ([]byte, error) {
	resp, err := c.api.GetThumbnail(ctx, &yt_tbv1.GetThumbnailRequest{VideoUrl: url})
	if err != nil {
		return nil, err
	}

	return resp.ThumbnailData, nil
}
