package grpc_client

import (
	"context"
	yt_tbv1 "github.com/frog-in-fog/youtube_thumbnail_protos/gen/yt_thumbnail/yt_thumbnail"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"log/slog"
)

// Client is the struct for gRPC client
type Client struct {
	log *slog.Logger
	api yt_tbv1.ThumbnailServiceClient
}

func NewClient(ctx context.Context, addr string, log *slog.Logger) (*Client, error) {
	clientConn, err := grpc.DialContext(ctx, addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	grpcClient := yt_tbv1.NewThumbnailServiceClient(clientConn)

	return &Client{log: log, api: grpcClient}, nil
}
