package main

import (
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/app"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/config"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/lib/logger"
	"os"
	"os/signal"
	"syscall"
)

func main() {
	cfg := config.MustLoad()

	log := logger.SetupLogger(cfg.Env)

	application := app.NewApp(log, cfg.GRPC.GRPCPort, cfg.StoragePath)

	go func() {
		log.Info("starting gRPC server")
		application.GRPCServer.MustRun()
	}()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)

	<-stop

	application.GRPCServer.Stop()
	log.Info("server stopped")
}
