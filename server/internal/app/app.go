package app

import (
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/app/grpc_app"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/service/thumbnail"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/storage/sqlite"
	"log/slog"
)

// App is the type for application in general
type App struct {
	GRPCServer *grpc_app.App
}

func NewApp(log *slog.Logger, grpcPort int, storagePath string) *App {
	// storage
	sqliteStorage, err := sqlite.NewSQLiteStorage(storagePath)
	if err != nil {
		panic(err)
	}
	// service
	tbnService := thumbnail.NewThumbnailService(log, sqliteStorage, sqliteStorage)

	// handlers
	grpcApp := grpc_app.NewGRPCApp(log, grpcPort, tbnService)

	return &App{GRPCServer: grpcApp}

}
