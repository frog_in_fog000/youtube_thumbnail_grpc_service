package grpc_app

import (
	"fmt"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/grpc_server"
	"google.golang.org/grpc"
	"log/slog"
	"net"
)

// App is the type of gRPC application
type App struct {
	log        *slog.Logger
	gRPCServer *grpc.Server
	port       int
}

func NewGRPCApp(log *slog.Logger, port int, thumbnailService grpc_server.ThumbnailService) *App {
	gRPCServer := grpc.NewServer()
	grpc_server.RegisterGrpcServer(gRPCServer, thumbnailService)

	return &App{
		log:        log,
		gRPCServer: gRPCServer,
		port:       port,
	}
}

// MustRun is the function that runs tcp and gRPC server of panics
func (a *App) MustRun() {
	if err := a.Run(); err != nil {
		panic(err)
	}
}

// Run is the function that runs tcp and gRPC server
func (a *App) Run() error {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", a.port))
	if err != nil {
		a.log.Info("failed to listen", "port", a.port)
		return err
	}

	a.log.Info("grpc_app server listening", "port", a.port)

	if err = a.gRPCServer.Serve(listener); err != nil {
		return err
	}

	return nil
}

// Stop stops gRPC server
func (a *App) Stop() {
	a.log.Info("stopping grpc_app server...")
	a.gRPCServer.GracefulStop()
}
