package config

import (
	"github.com/ilyakaznacheev/cleanenv"
	"time"
)

// Config is the type for config abstraction
type Config struct {
	Env         string `env:"ENV" envDefault:"local"`
	StoragePath string `env:"STORAGE_PATH" env-required:"true" envDefault:"./yt_tbn.db"`
	GRPC        struct {
		GRPCPort    int           `env:"GRPC_PORT" envDefault:"50051"`
		GRPCTimeout time.Duration `env:"GRPC_TIMEOUT" envDefault:"10s"`
	}
	MigrationsPath  string `env:"MIGRATIONS_PATH" env-required:"true" envDefault:"./migrations"`
	MigrationsTable string `env:"MIGRATIONS_TABLE" env-required:"true" envDefault:"thumbnails"`
}

// MustLoad function parses config successfully or panics
func MustLoad() *Config {
	var cfg Config
	if err := cleanenv.ReadEnv(&cfg); err != nil {
		panic("failed to load config: " + err.Error())
	}

	return &cfg
}
