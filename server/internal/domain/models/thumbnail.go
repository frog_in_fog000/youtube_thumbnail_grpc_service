package models

import "time"

type ThumbnailMeta struct {
	VideoID   string    `json:"url"`
	Data      []byte    `json:"data"`
	CreatedAt time.Time `json:"created_at"`
}
