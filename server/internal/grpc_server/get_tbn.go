package grpc_server

import (
	"context"
	yt_tbv1 "github.com/frog-in-fog/youtube_thumbnail_protos/gen/yt_thumbnail/yt_thumbnail"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// GetThumbnail is the implementation of the gRPC method
func (s *serverApi) GetThumbnail(ctx context.Context, req *yt_tbv1.GetThumbnailRequest) (*yt_tbv1.GetThumbnailResponse, error) {
	if req.GetVideoUrl() == "" {
		return nil, status.Errorf(codes.InvalidArgument, "video url is empty")
	}

	tbn, err := s.thumbnail.Thumbnail(ctx, req.GetVideoUrl())
	if err != nil {
		return nil, status.Errorf(codes.Internal, "internal error")
	}

	if tbn == nil {
		return &yt_tbv1.GetThumbnailResponse{}, nil
	}

	return &yt_tbv1.GetThumbnailResponse{ThumbnailData: tbn.Data}, nil
}
