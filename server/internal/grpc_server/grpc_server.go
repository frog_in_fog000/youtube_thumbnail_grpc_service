package grpc_server

import (
	"context"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/domain/models"
	yt_tbv1 "github.com/frog-in-fog/youtube_thumbnail_protos/gen/yt_thumbnail/yt_thumbnail"
	"google.golang.org/grpc"
)

// ThumbnailService is the interface for service
type ThumbnailService interface {
	Thumbnail(ctx context.Context, url string) (*models.ThumbnailMeta, error)
	DeleteThumbnailWithDelay(ctx context.Context, videoID string) error
}

type serverApi struct {
	yt_tbv1.UnimplementedThumbnailServiceServer
	thumbnail ThumbnailService
}

func RegisterGrpcServer(grpcServer *grpc.Server, thumbnail ThumbnailService) {
	yt_tbv1.RegisterThumbnailServiceServer(grpcServer, &serverApi{
		thumbnail: thumbnail,
	})
}
