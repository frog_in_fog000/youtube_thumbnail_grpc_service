package thumbnail

import (
	"context"
	"errors"
	"fmt"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/domain/models"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/storage"
	"io"
	"log/slog"
	"net/http"
	"net/url"
	"time"
)

type Thumbnail struct {
	log         *slog.Logger
	tbnSaver    TBNSaver
	tbnProvider TBNProvider
}

// TBNProvider is the interface for storage
type TBNProvider interface {
	GetThumbnail(ctx context.Context, videoID string) (*models.ThumbnailMeta, error)
	DeleteThumbnail(ctx context.Context, videoID string) error
}

// TBNSaver is the interface for storage
type TBNSaver interface {
	SaveThumbnail(ctx context.Context, thumbnail *models.ThumbnailMeta) error
}

func NewThumbnailService(log *slog.Logger, tbnSaver TBNSaver, tbnProvider TBNProvider) *Thumbnail {
	return &Thumbnail{
		log:         log,
		tbnSaver:    tbnSaver,
		tbnProvider: tbnProvider,
	}
}

// Thumbnail is the service to get thumbnail from video URL
func (t *Thumbnail) Thumbnail(ctx context.Context, videoURL string) (*models.ThumbnailMeta, error) {
	if videoURL == "" {
		t.log.Error("video url is empty", "url", videoURL)
		return nil, errors.New("video url is empty")
	}
	var thumbnail *models.ThumbnailMeta
	// check data in cache
	videoID, err := getVideoIDFromURL(videoURL)
	if err != nil {
		return nil, err
	}
	t.log.Info("checking data in cache...")
	tbnMeta, err := t.tbnProvider.GetThumbnail(ctx, videoID)
	if err != nil {
		if errors.Is(err, storage.ErrThumbnailNotFound) {
			// if not, send http request
			t.log.Info("no data in cache, fetching http request...", "data", tbnMeta)
			thumbnail, err = getThumbnailFromURL(videoURL)
			if err != nil {
				t.log.Error("failed to fetch thumbnail", "videoID", videoID, "err", err)
				return nil, err
			}
			// save data to cache
			if err = t.tbnSaver.SaveThumbnail(ctx, thumbnail); err != nil {
				t.log.Error("failed to save thumbnail", "videoID", videoID, "err", err)
				return nil, err
			}
			// launch timer in its own goroutine
			t.log.Info("setting new timer...", "videoID", videoID)
			go func() {
				if err = t.DeleteThumbnailWithDelay(ctx, thumbnail.VideoID); err != nil {
					t.log.Error("failed to delete thumbnail", "videoID", videoID, "err", err)
					return
				}
			}()
			return thumbnail, nil
		}
		t.log.Error("failed to get thumbnail from cache", "videoID", videoID, "err", err)
		return nil, err
	}
	// if data found in cache, return it immediately
	t.log.Info("data found in cache", "videoID", videoID)
	return tbnMeta, nil
}

// DeleteThumbnailWithDelay deletes thumbnail with delay
func (t *Thumbnail) DeleteThumbnailWithDelay(ctx context.Context, videoID string) error {
	<-time.After(30 * time.Second)

	if err := t.tbnProvider.DeleteThumbnail(ctx, videoID); err != nil {
		return err
	}

	t.log.Info("thumbnail deleted", "videoID", videoID)
	return nil
}

// getThumbnailFromURL gets video thumbnail from url string
func getThumbnailFromURL(videoUrl string) (*models.ThumbnailMeta, error) {
	var thumbnail models.ThumbnailMeta

	videoID, err := getVideoIDFromURL(videoUrl)
	if err != nil {
		return nil, err
	}
	data, err := makeHttpGetRequest(videoID)
	if err != nil {
		return nil, err
	}

	thumbnail.VideoID = videoID
	thumbnail.Data = data
	thumbnail.CreatedAt = time.Now()

	return &thumbnail, nil
}

// makeHttpGetRequest makes http get request to get image
func makeHttpGetRequest(videoId string) ([]byte, error) {
	targetUrl := fmt.Sprintf("https://img.youtube.com/vi/%s/0.jpg", videoId)
	req, err := http.NewRequest(http.MethodGet, targetUrl, nil)
	if err != nil {
		return nil, err
	}

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	respBody, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return respBody, nil
}

// getVideoIDFromURL gets video id from url
func getVideoIDFromURL(videoUrl string) (string, error) {
	parsedURL, err := url.Parse(videoUrl)
	if err != nil {
		return "", err
	}

	params, err := url.ParseQuery(parsedURL.RawQuery)
	if err != nil {
		return "", err
	}

	videoID := params.Get("v")
	if videoID == "" {
		return "", errors.New("video id is empty")
	}

	return videoID, nil
}
