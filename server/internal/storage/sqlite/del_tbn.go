package sqlite

import "context"

// DeleteThumbnail deletes thumbnail from storage
func (s *Storage) DeleteThumbnail(ctx context.Context, videoID string) error {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	stmt, err := s.db.Prepare(`DELETE FROM thumbnails WHERE video_id = ?`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.Exec(videoID)
	if err != nil {
		return err
	}

	return nil
}
