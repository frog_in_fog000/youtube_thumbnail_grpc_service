package sqlite

import (
	"context"
	"database/sql"
	"errors"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/domain/models"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/storage"
)

// GetThumbnail provides thumbnail by videoID
func (s *Storage) GetThumbnail(ctx context.Context, videoID string) (*models.ThumbnailMeta, error) {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	stmt, err := s.db.Prepare(`SELECT data FROM thumbnails WHERE video_id = ?`)
	if err != nil {
		return nil, err
	}
	defer stmt.Close()

	var thumbnail models.ThumbnailMeta

	if err = stmt.QueryRowContext(ctx, videoID).Scan(&thumbnail.Data); err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return &models.ThumbnailMeta{}, storage.ErrThumbnailNotFound
		}
		return nil, err
	}
	return &thumbnail, nil
}
