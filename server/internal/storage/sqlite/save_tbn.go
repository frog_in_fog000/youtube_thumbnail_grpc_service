package sqlite

import (
	"context"
	"github.com/frog-in-fog/youtube_thumbnail_grpc/internal/domain/models"
)

// SaveThumbnail saves thumbnail into storage
func (s *Storage) SaveThumbnail(ctx context.Context, thumbnail *models.ThumbnailMeta) error {
	s.mtx.Lock()
	defer s.mtx.Unlock()

	stmt, err := s.db.Prepare(`INSERT OR REPLACE INTO thumbnails (video_id, data, created_at) 
										VALUES (?, ?, ?)`)
	if err != nil {
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, thumbnail.VideoID, thumbnail.Data, thumbnail.CreatedAt)
	if err != nil {
		return err
	}

	return nil
}
