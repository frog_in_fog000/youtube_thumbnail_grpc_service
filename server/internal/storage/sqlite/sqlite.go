package sqlite

import (
	"database/sql"
	_ "github.com/mattn/go-sqlite3"
	"sync"
)

type Storage struct {
	db  *sql.DB
	mtx sync.Mutex
}

func NewSQLiteStorage(path string) (*Storage, error) {
	db, err := sql.Open("sqlite3", path)
	if err != nil {
		return nil, err
	}

	return &Storage{db: db}, nil
}

func (s *Storage) Close() {
	s.db.Close()
}
