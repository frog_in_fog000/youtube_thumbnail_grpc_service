package storage

import "errors"

var (
	ErrThumbnailNotFound = errors.New("thumbnail not found")
)
