CREATE TABLE IF NOT EXISTS thumbnails (
    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
    video_id VARCHAR(30) UNIQUE NOT NULL,
    data BLOB,
    created_at TIMESTAMP
);

CREATE INDEX IF NOT EXISTS idx_video_id ON thumbnails (video_id);