package tests

import (
	"github.com/frog-in-fog/youtube_thumbnail_grpc/tests/suite"
	yt_tbv1 "github.com/frog-in-fog/youtube_thumbnail_protos/gen/yt_thumbnail/yt_thumbnail"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"testing"
)

const (
	exampleVideoURL          = "https://www.youtube.com/watch?v=jNQXAC9IVRw"
	exampleIncorrectVideoURL = "https://www.google.com"
	emptyVideoURL            = ""
)

func TestThumbnail_HappyPath(t *testing.T) {
	ctx, st := suite.NewSuite(t)

	tbnResp, err := st.TbnClient.GetThumbnail(ctx, &yt_tbv1.GetThumbnailRequest{VideoUrl: exampleVideoURL})
	require.NoError(t, err)
	assert.NotNil(t, tbnResp)
}

func TestThumbnail_EmptyUrl(t *testing.T) {
	ctx, st := suite.NewSuite(t)

	tbnResp, err := st.TbnClient.GetThumbnail(ctx, &yt_tbv1.GetThumbnailRequest{VideoUrl: emptyVideoURL})
	require.Error(t, err)
	assert.Nil(t, tbnResp)
	assert.ErrorContains(t, err, "video url is empty")
}

func TestThumbnail_IncorrectUrl(t *testing.T) {
	ctx, st := suite.NewSuite(t)

	tbnResp, err := st.TbnClient.GetThumbnail(ctx, &yt_tbv1.GetThumbnailRequest{VideoUrl: exampleIncorrectVideoURL})
	require.Error(t, err)
	assert.Nil(t, tbnResp)
	assert.ErrorContains(t, err, "internal error")
}
