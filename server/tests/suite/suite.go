package suite

import (
	"context"
	yt_tbv1 "github.com/frog-in-fog/youtube_thumbnail_protos/gen/yt_thumbnail/yt_thumbnail"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"net"
	"strconv"
	"testing"
	"time"
)

type Suite struct {
	*testing.T
	TbnClient yt_tbv1.ThumbnailServiceClient
}

const (
	grpcHost    = "localhost"
	grpcPort    = 50051
	grpcTimeout = 10 * time.Second
)

func NewSuite(t *testing.T) (context.Context, *Suite) {
	t.Helper()
	t.Parallel()

	//cfg := config.MustLoad()

	ctx, cancelCtx := context.WithTimeout(context.Background(), grpcTimeout)

	t.Cleanup(func() {
		t.Helper()
		cancelCtx()
	})

	addr := net.JoinHostPort(grpcHost, strconv.Itoa(grpcPort))

	clientConn, err := grpc.DialContext(ctx, addr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		t.Fatalf("failed to dial grpc server: %v", err)
	}

	return ctx, &Suite{
		T:         t,
		TbnClient: yt_tbv1.NewThumbnailServiceClient(clientConn),
	}
}
